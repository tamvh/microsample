package main

import (
	"context"
	"gitlab.com/microsample/helper/config"
	"gitlab.com/microsample/helper/domain"
	helper "gitlab.com/microsample/helper/grpc"
	hdl "gitlab.com/microsample/middleware-service/middeware-sample/handler"
	pb "gitlab.com/microsample/protobuf/middleware/middeware-sample"
	"google.golang.org/grpc"
)

func main() {
	cfg := &config.ServiceBase{}
	helper.StartServer(domain.MiddlewareSample, cfg, func(ctx context.Context, s *grpc.Server) {
		pb.RegisterSampleAPIServer(s, hdl.NewHandler(ctx, cfg.GetConsul()))
	})
}
