package config

import "gitlab.com/microsample/helper/config"

type Service struct {
	config.ServiceBase
}

var (
	//ServiceCfg config for this project
	ServiceCfg = &Service{}
)
