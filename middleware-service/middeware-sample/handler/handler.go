package handler

import (
	"context"
	"github.com/golang/glog"
	"github.com/jinzhu/copier"
	"gitlab.com/microsample/core-service/core-service-samle/config"
	"gitlab.com/microsample/helper/domain"
	grpcHelper "gitlab.com/microsample/helper/grpc"
	sampleCoreService "gitlab.com/microsample/protobuf/core-service/core-service-sample"
	pb "gitlab.com/microsample/protobuf/middleware/middeware-sample"
)

type handler struct {
	sampleCoreServiceCli sampleCoreService.CoreServiceSampleClient
}

func NewHandler(ctx context.Context, consulAddr string) pb.SampleAPIServer {
	cfg := config.ServiceCfg
	return &handler{
		sampleCoreServiceCli: sampleCoreService.NewCoreServiceSampleClient(grpcHelper.CreateGrpcConn(ctx, cfg.GetConsul(), domain.CoreServiceSample)),
	}
}

func (h *handler) SampleFunc(ctx context.Context, req *pb.SampleRequest) (*pb.SampleResponse, error) {
	glog.Infoln(req)
	resp := &pb.SampleResponse{}
	coreReq := &sampleCoreService.SampleFuncRequest{}
	copier.Copy(coreReq, req)
	coreResp, err := h.sampleCoreServiceCli.SampleFunc(ctx, coreReq)
	if err != nil {
		glog.Errorln(err)
		return nil, err
	}
	copier.Copy(resp, coreResp)
	return resp, nil
}
