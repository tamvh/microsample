package main

import (
	"context"
	"gitlab.com/microsample/core-service/core-service-samle/config"
	hdl "gitlab.com/microsample/core-service/core-service-samle/handler"
	"gitlab.com/microsample/helper/domain"
	myGrpc "gitlab.com/microsample/helper/grpc"
	pb "gitlab.com/microsample/protobuf/core-service/core-service-sample"
	"google.golang.org/grpc"
)

func main() {
	myGrpc.StartServer(domain.CoreServiceSample, config.ServiceCfg, func(ctx context.Context, s *grpc.Server) {
		pb.RegisterCoreServiceSampleServer(s, hdl.NewHandler(ctx))
	})
}
