package handler

import (
	"context"
	"github.com/golang/glog"
	pb "gitlab.com/microsample/protobuf/core-service/core-service-sample"
)

type handler struct {
}

func NewHandler(ctx context.Context) pb.CoreServiceSampleServer {
	return &handler{

	}
}

func (h *handler) SampleFunc(ctx context.Context, req *pb.SampleFuncRequest) (*pb.SampleFuncResponse, error) {
	glog.Infoln(req)
	resp := &pb.SampleFuncResponse{
		Id: req.GetId(),
	}
	return resp, nil
}
