package gateway

import (
	"context"
	"fmt"
	"net/http"

	"github.com/golang/glog"
	"github.com/mwitkow/grpc-proxy/proxy"

	gwruntime "github.com/grpc-ecosystem/grpc-gateway/runtime"
	"gitlab.com/microsample/helper/domain"
	helper "gitlab.com/microsample/helper/grpc"
	middlewareSample "gitlab.com/microsample/protobuf/middleware/middeware-sample"
	"google.golang.org/grpc"
)

type cbFn func(context.Context, *gwruntime.ServeMux, *grpc.ClientConn) error
type registerParam struct {
	consulKey, serviceName string
	fn                     cbFn
}

func registerServiceAPIHandler(ctx context.Context, mux *gwruntime.ServeMux, consulAdd string, param registerParam) {
	conn := helper.CreateGrpcConn(ctx, consulAdd, param.consulKey)
	if err := param.fn(ctx, mux, conn); err != nil {
		glog.Errorln(err)
		return
	}
	if param.serviceName != "" {
		grpcConnTbl[param.serviceName] = helper.CreateProxyGrpcConn(ctx, consulAdd, param.consulKey, proxy.Codec())

	} else {
		glog.Errorln("registerServiceAPIHandler " + param.consulKey + " Empty")
	}

}

// NewGateway returns a new gateway server which translates HTTP into gRPC.
func NewGateway(ctx context.Context, opts []gwruntime.ServeMuxOption, consulAdd string, grpcPort int) (http.Handler, error) {
	marshalOption := gwruntime.WithMarshalerOption(gwruntime.MIMEWildcard, &gwruntime.JSONPb{})
	opts = append(opts, marshalOption)
	mux := gwruntime.NewServeMux(opts...)

	registerParams := []registerParam{
		{serviceName: "api.sample.SampleAPI", fn: middlewareSample.RegisterSampleAPIHandler, consulKey: domain.MiddlewareSample},
	}
	for _, param := range registerParams {
		registerServiceAPIHandler(ctx, mux, consulAdd, param)
	}
	startGrpcProxy(fmt.Sprintf("%d", grpcPort))

	return mux, nil
}
