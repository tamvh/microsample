package gateway

import (
	"bytes"
	"io/ioutil"
	"log"
	"net/http"
	"strings"
	"time"

	"gitlab.com/microsample/helper/kafka"
)

func ProxyServer(proxyUrl string, proxyPrefix string, kafka kafka.Writer) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		requestDate := time.Now()
		client := &http.Client{
			Timeout: time.Duration(30 * time.Second),
		}
		target := strings.Replace(r.RequestURI, proxyPrefix, "", 1)
		reqBody, err := ioutil.ReadAll(r.Body)
		request := map[string]interface{}{
			"host":        r.Host,
			"method":      r.Method,
			"url":         strings.Replace(r.URL.String(), proxyPrefix, "", 1),
			"header":      r.Header,
			"body":        string(reqBody),
			"requestDate": requestDate,
		}
		req, err := http.NewRequest(r.Method, proxyUrl+target, bytes.NewReader(reqBody))
		for name, value := range r.Header {
			req.Header.Set(name, value[0])
		}
		resp, err := client.Do(req)
		r.Body.Close()

		response := map[string]interface{}{
			"header":       resp.Header,
			"code":         resp.StatusCode,
			"responseDate": time.Now(),
		}

		if err != nil {
			log.Println(err)
			response["body"] = err
			data := map[string]interface{}{
				"request":  request,
				"response": response,
			}
			sendToKafka(kafka, data)

			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}
		for k, v := range resp.Header {
			w.Header().Set(k, v[0])
		}

		body, err := ioutil.ReadAll(resp.Body)
		bodyString := string(body)

		w.WriteHeader(resp.StatusCode)
		w.Write([]byte(bodyString))
		response["body"] = bodyString

		data := map[string]interface{}{
			"request":  request,
			"response": response,
		}

		sendToKafka(kafka, data)

		resp.Body.Close()
	}
}

func sendToKafka(kafka kafka.Writer, data interface{}) {

}
