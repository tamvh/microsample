package gateway

import (
	"context"
	"net"
	"strings"

	"github.com/golang/glog"
	"github.com/mwitkow/grpc-proxy/proxy"
	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/metadata"
)

var (
	grpcConnTbl map[string]*grpc.ClientConn
)

func init() {
	grpcConnTbl = make(map[string]*grpc.ClientConn)
}
func startGrpcProxy(port string) error {
	proxyListener, err := net.Listen("tcp", ":"+port)
	if err != nil {
		glog.Error(err)
		return err
	}
	director := func(ctx context.Context, fullName string) (context.Context, *grpc.ClientConn, error) {
		glog.Infoln(fullName)
		md, ok := metadata.FromIncomingContext(ctx)
		outCtx, _ := context.WithCancel(ctx)
		outCtx = metadata.NewOutgoingContext(outCtx, md.Copy())
		glog.Infoln("outCtx: ", outCtx)
		if ok {
			methods := strings.SplitN(fullName, "/", 3)
			if len(methods) < 2 || methods[0] != "" {
				return nil, nil, grpc.Errorf(codes.InvalidArgument, "Invalid Param")
			}
			if conn, ok := grpcConnTbl[methods[1]]; ok {
				return outCtx, conn, nil
			}
		}
		return nil, nil, grpc.Errorf(codes.Unimplemented, "Unknown method")
	}
	proxySr := grpc.NewServer(
		grpc.CustomCodec(proxy.Codec()),
		grpc.UnknownServiceHandler(proxy.TransparentHandler(director)),
	)

	glog.Infoln("start grpcProxy")

	go proxySr.Serve(proxyListener)
	return nil

}
