#!/bin/bash

me=`basename "$0"`
sme="\.\/$me "
if [ "$1" != "" ]; then
	docker-compose stop $1
	docker-compose pull $1
	docker-compose up -d $1
else
	echo "Service name is empty!"
	docker ps --format '{{.Names}}' | grep "dungtd_" | sed -e "s/_1//g" | sed -e "s/dungtd_/$sme/g"
fi