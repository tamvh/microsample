package errorutil

import (
	grpcCodes "google.golang.org/grpc/codes"
	grpcStatus "google.golang.org/grpc/status"
)

const (
	ERROR_SYSTEM            = "Lỗi hệ thống"
	ERROR_INVALID_DATA      = "Dữ liệu không hợp lệ"
	ERROR_PERMISSION_DENIED = "Permission denied"
	ERROR_TOKEN_EXPIRED     = "Phiên đăng nhập hết hạn hoặc không tồn tại"
	ERROR_MERCHANT_INVALID  = "Merchant không hợp lệ"
	ERROR_NOT_FOUND         = "Không tồn tại"
)

var (
	ErrorSystem          = grpcStatus.Errorf(grpcCodes.Internal, ERROR_SYSTEM)
	ErrorPermission      = grpcStatus.Errorf(grpcCodes.PermissionDenied, ERROR_PERMISSION_DENIED)
	ErrorInvalidData     = grpcStatus.Errorf(grpcCodes.InvalidArgument, ERROR_INVALID_DATA)
	ErrorTokenExpired    = grpcStatus.Errorf(grpcCodes.Unauthenticated, ERROR_TOKEN_EXPIRED)
	ErrorMerchantInvalid = grpcStatus.Errorf(grpcCodes.InvalidArgument, ERROR_MERCHANT_INVALID)
)
