#!/bin/bash
protoc -I/usr/local/include -I. \
  -I$GOPATH/src \
  --go_out=.\
  *.proto &&
  protoc-go-inject-tag --input=common.pb.go