package crypto


import (
	"crypto/hmac"
	"crypto/sha256"
	"fmt"
	"strings"
)

//Sha256Sum ...
func Sha256Sum(sep string, params ...string) string {
	return fmt.Sprintf("%x", sha256.Sum256([]byte(strings.Join(params, sep))))
}

//HmacSha256Sum ...
func HmacSha256Sum(key, sep string, params ...string) string {
	sig := hmac.New(sha256.New, []byte(key))
	sig.Write([]byte(strings.Join(params, sep)))
	return fmt.Sprintf("%x", sig.Sum(nil))
}
