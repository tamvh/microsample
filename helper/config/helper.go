package config

import (
	"errors"
	"os"

	"github.com/golang/glog"

	"gitlab.com/microsample/helper/consul"
	"github.com/BurntSushi/toml"
)

//Redis ....
type Redis struct {
	Addr, Password string
	DB             int
}

//Database ...
type Database struct {
	Addr, User, Password, Databasename, Driver string
}

//Kafka ...
type Kafka struct {
	Addrs           []string
	Topics          []string
	Group           string
	Oldest          bool
	MaxMessageBytes int
	Compress        bool
}

//Elastic ...
type Elastic struct {
	Addr  string
	Sniff bool
}

//Sms ...
type Sms struct {
	EndPoint  string
	SecretKey string
	ClientID  string
	BrandName string
}

//Service ...
type Service interface {
	GetConsul() string
	GetOpenTracing() string
	GetPort() int
	Getenv(serviceName string)
}

//ServiceBase ...
type ServiceBase struct {
	Consul, OpenTracing string
	Port                int
	Database            Database
	Redis               Redis
	Kafka               Kafka
	GrpcPort            int
}

//GetRedis ...
func (s *ServiceBase) GetRedis() Redis {
	if s == nil {
		return Redis{}
	}
	return s.Redis
}

//GetConsul ...
func (s *ServiceBase) GetConsul() string {
	if s == nil {
		return ""
	}
	return s.Consul
}

//GetOpenTracing ...
func (s *ServiceBase) GetOpenTracing() string {
	if s == nil {
		return ""
	}
	return s.OpenTracing
}

//GetPort ...
func (s *ServiceBase) GetPort() int {
	if s == nil {
		return 0
	}
	return s.Port
}
func getEnv(val *string, key string) {
	env := os.Getenv(key)
	if len(env) > 0 {
		*val = env
	}
}

//Getenv ...
func (s *ServiceBase) Getenv(serviceName string) {
	if s == nil {
		return
	}
	glog.Infoln(s)
	glog.Infoln("Getenv serviceName: ", serviceName)
	getEnv(&s.Consul, "CONSUL_ADDRESS")
	if len(s.Consul) == 0 {
		s.Consul = "127.0.0.1:8500"
	}
	getEnv(&s.OpenTracing, "OPEN_TRACING_ADDRESS")
	getEnv(&s.Database.Databasename, "DB_NAME")
	getEnv(&s.Database.Addr, "DB_ADDR")
	getEnv(&s.Database.Password, "DB_PASS")
	getEnv(&s.Database.User, "DB_USR")
	getEnv(&s.Redis.Addr, "REDIS_ADDR")
	getEnv(&s.Redis.Password, "REDIS_PASS")

	s.Kafka.Group = serviceName

}

//Init ...
func Init(serviceName string, s Service) error {

	defer s.Getenv(serviceName)

	if s == nil || len(serviceName) == 0 {
		return errors.New("Empty serviceName")
	}
	consulAddr := consul.GetAddress("127.0.0.1:8500")
	if consul.GetConfig(consulAddr, serviceName, s) != nil {
		if _, err := toml.DecodeFile("config.toml", s); err != nil {
			return err
		}
	}

	return nil
}