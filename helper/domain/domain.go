package domain

import (
	"os"
)

var (
	ApiGateWay               = os.Getenv("SERVICE_PREFIX_NAME") + "microsample.api-gateway"
	MiddlewareAuthentication = os.Getenv("SERVICE_PREFIX_NAME") + "microsample.middleware.authentication"
	MiddlewareSample         = os.Getenv("SERVICE_PREFIX_NAME") + "microsample.middleware.sample"
	CoreServiceSample        = os.Getenv("SERVICE_PREFIX_NAME") + "microsample.core.service.sample"
)
