package location

import (
	"time"

	"github.com/hailocab/go-geoindex"
)

var (
	secondsOfVietNam = int((7 * time.Hour).Seconds())
	//VNLocation ...
	VNLocation = time.FixedZone("Vietnam", secondsOfVietNam)
)

type GeoPoint struct {
	id     string
	lat    float64
	lon    float64
	accept bool
}

func (g *GeoPoint) Lat() float64 { return g.lat }
func (g *GeoPoint) Lon() float64 { return g.lon }
func (g *GeoPoint) Id() string   { return g.id }
func (g *GeoPoint) Accept() bool { return g.accept }

type LocationFinder struct {
	index *geoindex.PointsIndex
}

func (l *LocationFinder) InitIndex(km float64) {
	l.index = geoindex.NewPointsIndex(geoindex.Km(km))
}

func (l *LocationFinder) Add(point *GeoPoint) {
	if l.index == nil {
		return
	}

	l.index.Add(point)
}

func (l *LocationFinder) Remove(id string) {
	if l.index == nil {
		return
	}

	l.index.Remove(id)
}

func (l *LocationFinder) Find(point *GeoPoint, k int, maxDistance float64) []string {
	if l.index == nil {
		return []string{}
	}

	points := l.index.KNearest(point, k, geoindex.Km(maxDistance), func(p geoindex.Point) bool {
		return p.(*GeoPoint).Accept()
	})

	ids := make([]string, len(points))
	for index, point := range points {
		ids[index] = point.Id()
	}

	return ids
}
