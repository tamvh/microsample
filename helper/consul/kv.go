package consul

import (
	"github.com/golang/glog"

	"fmt"

	"github.com/BurntSushi/toml"
	"github.com/hashicorp/consul/api"
)

//GetConfig ...
func GetConfig(address string, path string, out interface{}) error {
	value, err := getValue(address, path)

	if err != nil {
		glog.Error(err)
		return err
	}

	// Parse toml config
	if _, err = toml.Decode(value, out); err != nil {
		glog.Error(err)
		return err
	}

	return nil
}

func getValue(address string, path string) (string, error) {
	// Get a new client
	config := api.DefaultConfig()
	config.Address = address
	client, err := api.NewClient(config)
	if err != nil {
		glog.Error(err)
		return "", err
	}

	// Get a handle to the KV API
	kv := client.KV()

	// Lookup the pair
	pair, _, err := kv.Get(path, nil)
	if err != nil || pair == nil || len(pair.Value) == 0 {
		if err == nil {
			err = fmt.Errorf("Consul empty")
		}
		glog.Infoln(path, err)
		return "", err
	}

	return string(pair.Value), nil
}
