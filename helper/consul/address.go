package consul

import "os"

//GetAddress :
func GetAddress(addr string) string {
	consul := os.Getenv("CONSUL_ADDRESS")

	if len(consul) == 0 {
		if len(addr) == 0 {
			consul = "127.0.0.1:8500"
		} else {
			consul = addr
		}
	}
	return consul
}
