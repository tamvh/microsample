package consul

import (
	"time"

	"github.com/golang/glog"
	wonaming "github.com/wothing/wonaming/consul"
)

//AddServiceToConsul ...
func AddServiceToConsul(serviceName string) error {
	err := wonaming.Register(serviceName, "", 0, GetAddress(""), time.Second*10, 15)
	if err != nil {
		glog.Error(err)
	}
	return err
}
