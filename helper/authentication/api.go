package authentication

import (
	"context"
	"crypto/md5"
	"fmt"
	"reflect"
	"strings"

	helperCrypto "gitlab.com/microsample/helper/crypto"
	"github.com/golang/glog"
	"github.com/grpc-ecosystem/go-grpc-middleware/auth"
	grpcCodes "google.golang.org/grpc/codes"
	grpcStatus "google.golang.org/grpc/status"
)

type MethodList []string

type MiddlewareServiceAPIKeyAuth struct {
	MethodWhiteList MethodList
	SalKey          string
}

func getSigValues(req interface{}) ([]string, error) {
	var sigs []string
	val := reflect.ValueOf(req)
	if val.Kind() == reflect.Ptr {
		if val.IsNil() {
			glog.Errorln("nil ...")
			return sigs, fmt.Errorf("req is nil")
		}
		val = val.Elem()
	}
	vt := val.Type()
	for i := 0; i < vt.NumField(); i++ {
		field := vt.Field(i)
		if _, ok := field.Tag.Lookup("sig"); ok {
			sigs = append(sigs, fmt.Sprintf("%v", val.FieldByName(field.Name).Interface()))

		}
	}
	return sigs, nil
}
func (h *MiddlewareServiceAPIKeyAuth) AuthorizeAPIKeyValidate(ctx context.Context, req interface{}, fullMethodName string) (context.Context, error) {
	for _, method := range h.MethodWhiteList {
		if isMethodName(fullMethodName, method) == true {
			return ctx, nil
		}
	}
	apiKey, err := grpc_auth.AuthFromMD(ctx, "bearer")
	if err != nil {
		return ctx, err
	}
	params := strings.SplitN(apiKey, ";", 2)
	if len(params) != 2 {
		glog.Errorln("Incorrect len bear: ", params)
		return ctx, grpcStatus.Errorf(grpcCodes.InvalidArgument, "Incorrect len bear: %d", len(params))
	}
	sigParams, err := getSigValues(req)
	if err != nil {
		return ctx, err
	}
	privateKey := fmt.Sprintf("%x", md5.Sum([]byte(params[0]+h.SalKey)))
	if params[1] != helperCrypto.HmacSha256Sum(privateKey, "|", sigParams...) {
		glog.Errorln(fmt.Sprintf("InCorrect checksum: privateKey = %s, publicKey = %s, reqHash = %s, sigParams = %s",
			privateKey, params[0], params[1], strings.Join(sigParams, "|")))
		return ctx, grpcStatus.Errorf(grpcCodes.InvalidArgument, "InCorrect checksum")
	}
	return ctx, nil
}