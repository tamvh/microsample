package grpc

import (
	"context"
	"flag"
	"fmt"
	"net"
	"os"
	"strconv"
	"time"

	"github.com/golang/glog"

	"gitlab.com/microsample/helper/config"
	"github.com/grpc-ecosystem/go-grpc-middleware"
	"github.com/grpc-ecosystem/go-grpc-middleware/tracing/opentracing"
	"github.com/grpc-ecosystem/go-grpc-middleware/validator"

	authen "gitlab.com/microsample/helper/authentication"
	net_helper "gitlab.com/microsample/helper/net"
	"gitlab.com/microsample/helper/opentracing"
	grpc_retry "github.com/grpc-ecosystem/go-grpc-middleware/retry"
	wonaming "github.com/wothing/wonaming/consul"
	"google.golang.org/grpc"
	"google.golang.org/grpc/reflection"
)

type callBackGrpc func(context.Context, *grpc.Server)

//StartServer ..
func StartServer(serviceName string, cfg config.Service, cb callBackGrpc) {
	os.Args = append(os.Args, "-logtostderr=true")
	flag.Parse()
	if err := config.Init(serviceName, cfg); err != nil {
		glog.Errorln("Init fail ", err)
	}

	glog.Infoln("Config ", cfg)
	opentracing.Init(cfg.GetOpenTracing(), serviceName)

	grpcServer := grpc.NewServer(grpc.StreamInterceptor(grpc_middleware.ChainStreamServer(
		grpc_opentracing.StreamServerInterceptor(),
		authen.StreamServerInterceptor(),
		grpc_validator.StreamServerInterceptor(),
	)),
		grpc.UnaryInterceptor(grpc_middleware.ChainUnaryServer(
			grpc_opentracing.UnaryServerInterceptor(),
			authen.UnaryServerInterceptor(),
			grpc_validator.UnaryServerInterceptor(),
		)), grpc.MaxSendMsgSize(64<<20))
	ctx := context.Background()
	ctx, cancel := context.WithCancel(ctx)
	defer cancel()
	cb(ctx, grpcServer)
	//sc.RegisterOrderServer(grpcServer, sc.NewServiceHandler(ctx, &config))
	lis, err := net.Listen("tcp", fmt.Sprintf(":%d", cfg.GetPort()))
	if err != nil {
		glog.Fatalf("failed to listen: %v", err)
	}
	host, _ := net_helper.ExtractRealIP("")
	_, port, _ := net.SplitHostPort(lis.Addr().String())

	strPort, _ := strconv.Atoi(port)
	if err := wonaming.Register(serviceName, host, strPort, cfg.GetConsul(), time.Second*10, 15); err != nil {
		glog.Error(err)
	}
	reflection.Register(grpcServer)
	if err := grpcServer.Serve(lis); err != nil {
		glog.Fatalf("failed to serve: %v", err)
	}
}

//CreateGrpcConn ...
func CreateGrpcConn(ctx context.Context, consulAdd, serviceName string) *grpc.ClientConn {
	r := wonaming.NewResolver(serviceName)
	b := grpc.RoundRobin(r)
	opts := []grpc.DialOption{grpc.WithInsecure(), grpc.WithBalancer(b),
		grpc.WithUnaryInterceptor(grpc_opentracing.UnaryClientInterceptor()),
		grpc.WithStreamInterceptor(grpc_opentracing.StreamClientInterceptor()),
		grpc.WithMaxMsgSize(64 << 20),
	}
	conn, err := grpc.Dial(consulAdd, opts...)
	if err != nil {
		return nil
	}
	defer func() {
		if err != nil {
			if cerr := conn.Close(); cerr != nil {
				glog.Infof("Failed to close conn to %s: %v", consulAdd, cerr)
			}
			return
		}
		go func() {
			<-ctx.Done()
			if cerr := conn.Close(); cerr != nil {
				glog.Infof("Failed to close conn to %s: %v", consulAdd, cerr)
			}
		}()
	}()
	return conn
}

//CreateProxyGrpcConn ...
func CreateProxyGrpcConn(ctx context.Context, consulAdd, serviceName string, codec grpc.Codec) *grpc.ClientConn {
	r := wonaming.NewResolver(serviceName)
	b := grpc.RoundRobin(r)
	opts := []grpc.DialOption{grpc.WithInsecure(), grpc.WithBalancer(b),
		grpc.WithUnaryInterceptor(grpc_opentracing.UnaryClientInterceptor()),
		grpc.WithStreamInterceptor(grpc_opentracing.StreamClientInterceptor()),
		grpc.WithMaxMsgSize(64 << 20),
		grpc.WithCodec(codec),
	}
	conn, err := grpc.Dial(consulAdd, opts...)
	if err != nil {
		return nil
	}
	defer func() {
		if err != nil {
			if cerr := conn.Close(); cerr != nil {
				glog.Infof("Failed to close conn to %s: %v", consulAdd, cerr)
			}
			return
		}
		go func() {
			<-ctx.Done()
			if cerr := conn.Close(); cerr != nil {
				glog.Infof("Failed to close conn to %s: %v", consulAdd, cerr)
			}
		}()
	}()
	return conn
}

//CreateGrpcConn ...
func CreateGrpcConnWithRetry(ctx context.Context, consulAdd, serviceName string) *grpc.ClientConn {
	r := wonaming.NewResolver(serviceName)
	b := grpc.RoundRobin(r)
	opts := []grpc.DialOption{grpc.WithInsecure(), grpc.WithBalancer(b),
		grpc.WithUnaryInterceptor(grpc_opentracing.UnaryClientInterceptor()),
		grpc.WithStreamInterceptor(grpc_opentracing.StreamClientInterceptor()),
		grpc.WithStreamInterceptor(grpc_retry.StreamClientInterceptor()),
		grpc.WithUnaryInterceptor(grpc_retry.UnaryClientInterceptor()),
	}
	conn, err := grpc.Dial(consulAdd, opts...)
	if err != nil {
		return nil
	}
	defer func() {
		if err != nil {
			if cerr := conn.Close(); cerr != nil {
				glog.Infof("Failed to close conn to %s: %v", consulAdd, cerr)
			}
			return
		}
		go func() {
			<-ctx.Done()
			if cerr := conn.Close(); cerr != nil {
				glog.Infof("Failed to close conn to %s: %v", consulAdd, cerr)
			}
		}()
	}()
	return conn
}

//CreateGrpcBlockConn create connection block
func CreateGrpcBlockConn(ctx context.Context, consulAdd, serviceName string) *grpc.ClientConn {
	r := wonaming.NewResolver(serviceName)
	b := grpc.RoundRobin(r)
	opts := []grpc.DialOption{grpc.WithInsecure(), grpc.WithBlock(), grpc.WithBalancer(b),
		grpc.WithUnaryInterceptor(grpc_opentracing.UnaryClientInterceptor()),
		grpc.WithStreamInterceptor(grpc_opentracing.StreamClientInterceptor())}
	conn, err := grpc.Dial(consulAdd, opts...)
	if err != nil {
		return nil
	}
	defer func() {
		if err != nil {
			if cerr := conn.Close(); cerr != nil {
				glog.Infof("Failed to close conn to %s: %v", consulAdd, cerr)
			}
			return
		}
		go func() {
			<-ctx.Done()
			if cerr := conn.Close(); cerr != nil {
				glog.Infof("Failed to close conn to %s: %v", consulAdd, cerr)
			}
		}()
	}()
	return conn
}