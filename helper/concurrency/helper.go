package concurrency

type concurrencyFunc func() error

//Concurrency ...
type Concurrency struct {
	f []concurrencyFunc
}

//New ...
func New() *Concurrency {
	return &Concurrency{}
}

//Add ...
func (cc *Concurrency) Add(f ...concurrencyFunc) *Concurrency {
	cc.f = append(cc.f, f...)
	return cc
}

//Do ...
func (cc *Concurrency) Do() error {
	return Do(cc.f...)
}

//Do ...
func Do(ccf ...concurrencyFunc) error {
	len := len(ccf)
	if len == 0 {
		return nil
	} else if len == 1 {
		return ccf[0]()
	}
	ec := make(chan error, len)
	for _, _f := range ccf {
		f := _f
		go func() { ec <- f() }()
	}
	for e := range ec {
		if e != nil {
			return e
		}
		if len--; len == 0 {
			return nil
		}
	}

	return nil

}
